// Copyright Epic Games, Inc. All Rights Reserved.

#include "ArenaShooterGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ArenaShooterGame, "ArenaShooterGame" );
 