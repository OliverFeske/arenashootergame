// Fill out your copyright notice in the Description page of Project Settings.


#include "ArenaShooterGameMode.h"
#include "UI/ArenaShooterHUD.h"
#include "Player/CharacterBase.h"
#include "UObject/ConstructorHelpers.h"

AArenaShooterGameMode::AArenaShooterGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Characters/Player/Base/BP_CharacterBase"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AArenaShooterHUD::StaticClass();

	bUseSeamlessTravel = true;
}