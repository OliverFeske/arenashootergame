// Fill out your copyright notice in the Description page of Project Settings.


#include "Online/ArenaShooterBeaconClient.h"

AArenaShooterBeaconClient::AArenaShooterBeaconClient()
{

}

bool AArenaShooterBeaconClient::ConnectToServer(const FString& Address)
{
	FURL Destination = FURL(nullptr, *Address, ETravelType::TRAVEL_Absolute);
	Destination.Port = 7787;

	return InitClient(Destination);
}

void AArenaShooterBeaconClient::LeaveLobby()
{
	DestroyBeacon();
}

void AArenaShooterBeaconClient::OnFailure()
{
	UE_LOG(LogTemp, Warning, TEXT("Client failed to connect to host beacon"));
	D_OnConnected.Broadcast(false);
}

void AArenaShooterBeaconClient::OnConnected()
{
	UE_LOG(LogTemp, Warning, TEXT("Client successfully connected to host beacon"));
	D_OnConnected.Broadcast(true);
}

void AArenaShooterBeaconClient::Client_OnDisconnected_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("Disconnected"));
	D_OnDisconnected.Broadcast();
}