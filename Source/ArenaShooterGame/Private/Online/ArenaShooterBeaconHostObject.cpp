// Fill out your copyright notice in the Description page of Project Settings.


#include "Online/ArenaShooterBeaconHostObject.h"
#include "Online/ArenaShooterBeaconClient.h"
#include "ArenaShooterMainMenuGameMode.h"

#include "OnlineBeaconHost.h"

AArenaShooterBeaconHostObject::AArenaShooterBeaconHostObject()
{
	ClientBeaconActorClass = AArenaShooterBeaconClient::StaticClass();
	BeaconTypeName = ClientBeaconActorClass->GetName();
}

void AArenaShooterBeaconHostObject::OnClientConnected(AOnlineBeaconClient* NewClientActor, UNetConnection* ClientConnection)
{
	Super::OnClientConnected(NewClientActor, ClientConnection);

	if (NewClientActor)
	{
		UE_LOG(LogTemp, Warning, TEXT("Connected client valid"));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Connected client invalid"));
	}
}

void AArenaShooterBeaconHostObject::NotifyClientDisconnected(AOnlineBeaconClient* LeavingClientActor)
{
	Super::NotifyClientDisconnected(LeavingClientActor);

	UE_LOG(LogTemp, Warning, TEXT("Client has disconnected"));
}

void AArenaShooterBeaconHostObject::DisconnectClient(AOnlineBeaconClient* ClientActor)
{
	AOnlineBeaconHost* BeaconHost = Cast<AOnlineBeaconHost>(GetOwner());
	if (BeaconHost)
	{
		if (AArenaShooterBeaconClient* ClientBeacon = Cast<AArenaShooterBeaconClient>(ClientActor))
		{
			ClientBeacon->Client_OnDisconnected();
		}
		BeaconHost->DisconnectClient(ClientActor);
	}
}

void AArenaShooterBeaconHostObject::ShutDownServer()
{
	// Unregister server via web api
	DisconnectAllClients();

	if (AOnlineBeaconHost* HostBeacon = Cast<AOnlineBeaconHost>(GetOwner()))
	{
		HostBeacon->UnregisterHost(BeaconTypeName);
		HostBeacon->DestroyBeacon();
	}
}

void AArenaShooterBeaconHostObject::DisconnectAllClients()
{
	UE_LOG(LogTemp, Warning, TEXT("Disconnecting all clients"));
	for (AOnlineBeaconClient* Client : ClientActors)
	{
		if (Client)
			DisconnectClient(Client);
	}
}