// Fill out your copyright notice in the Description page of Project Settings.


#include "ArenaShooterMainMenuGameMode.h"
#include "Online/ArenaShooterBeaconHostObject.h"

#include "OnlineBeaconHost.h"

AArenaShooterMainMenuGameMode::AArenaShooterMainMenuGameMode()
{

}

bool AArenaShooterMainMenuGameMode::CreateHostBeacon()
{
	if (AOnlineBeaconHost* HostBeacon = GetWorld()->SpawnActor<AOnlineBeaconHost>(AOnlineBeaconHost::StaticClass()))
	{
		if (HostBeacon->InitHost())
		{
			HostBeacon->PauseBeaconRequests(false);

			HostObject = GetWorld()->SpawnActor<AArenaShooterBeaconHostObject>(AArenaShooterBeaconHostObject::StaticClass());
			if (HostObject)
			{
				HostBeacon->RegisterHost(HostObject);
				return true;
			}
		}
	}

	return false;
}