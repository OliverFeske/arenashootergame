// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "ArenaShooterHUD.generated.h"

/**
 * 
 */
UCLASS()
class ARENASHOOTERGAME_API AArenaShooterHUD : public AHUD
{
	GENERATED_BODY()

public:
	AArenaShooterHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;
};
